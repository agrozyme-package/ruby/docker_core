Gem::Specification.new do

  # @type [Gem::Specification] gem
|gem|
  gem.version = '0.0.49'
  gem.name = 'docker_core'
  gem.summary = 'some functions for docker build / run'
  gem.authors = ['agrozyme']
  gem.files = Dir['lib/*.rb']
  gem.homepage = 'https://gitlab.com/agrozyme-package/ruby/docker_core'
  gem.required_ruby_version = '>= 2.5.0'
  gem.licenses = ['Ruby']
  gem.add_runtime_dependency('fileutils', '< 2')
  gem.add_runtime_dependency('json', '< 3')
  gem.add_runtime_dependency('thor', '< 2')
  gem.add_runtime_dependency('zlib', '< 4')
end
