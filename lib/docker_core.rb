require('etc')
require('fileutils')
require('json')
require('net/http')
require('rubygems/package')
require('thor')
require('yaml')
require('zlib')

module DockerCore
  SUDO = false
  USER = 'core'
  GROUP = 'core'
  REGISTRY = 'docker.io'
  NAMESPACE = 'library'

  module Base
    module Command
      module ClassMethod
        # @return [String]
        def from_image
          Error.no_method(__method__)
        end

        # @return [String]
        def work_folder
          Error.no_method(__method__)
        end

        # @param [Array] arguments
        # @param [Boolean] throw
        # @param [Numeric] wait
        # @param [Boolean] strip
        # @param [Boolean] echo
        def capture_command(*arguments, environment: {}, bind: {}, throw: false, wait: 0, strip: true, echo: false)
          bind[Dir.pwd] = self.work_folder
          Swarm.capture_command(self.from_image, *arguments, environment: environment, bind: bind, throw: throw, wait: wait, strip: strip, echo: echo)
        end

        # @param [Array] arguments
        # @param [Boolean] throw
        # @param [Numeric] wait
        # @param [Boolean] echo
        def run_command(*arguments, environment: {}, bind: {}, throw: true, wait: 0, echo: true)
          bind[Dir.pwd] = self.work_folder
          Swarm.run_command(self.from_image, *arguments, environment: environment, bind: bind, throw: throw, wait: wait, echo: echo)
        end
      end

      def self.included(base)
        base.extend(ClassMethod)
      end
    end
  end

  module Error
    # @param [Symbol] method
    def self.no_method(method)
      raise("#{method} method should be implemented in concrete class")
    end

    def self.maximum_redirect
      raise('maximum redirect reached')
    end
  end

  class Color < Thor::Shell::Color
    # @param [String] text
    # @param [Array<String>] colors
    def self.echo(text, *colors)
      if 0 == colors.size
        return puts(text)
      end

      color = colors.map do |color|
        if color.is_a?(Symbol)
          next self.const_get(color.to_s.upcase)
        end

        next "#{color}"
      end.join

      return puts("#{color}#{text}#{self::CLEAR}")
    end
  end

  class Runner < Thor
  end

  module Image
  end

  module Orchestrator
  end

  module Command
  end

  module Swarm
    # @param [String] image
    # @param [String] registry
    # @param [String] namespace
    # @param [String] tag
    def self.from_image(image, registry: REGISTRY, namespace: NAMESPACE, tag: 'latest')
      return "#{registry}/#{namespace}/#{image}:#{tag}"
    end

    def self.deploy_path
      return File.expand_path('~/deploy')
    end

    def self.swarm_path
      return File.expand_path('~/.swarm')
    end

    def self.profile_path
      return File.realpath('profile.sh', self.deploy_path)
    end

    def self.runner_path
      return File.realpath('runner.rb', self.deploy_path)
    end

    # @see  https://github.com/moby/moby/issues/31564
    # @see  https://www.geeksforgeeks.org/how-to-validate-a-domain-name-using-regular-expression
    # @param [String] name
    # @param [Boolean] echo
    def self.check_name(name, echo: false)
      color = Color::YELLOW
      name = "#{name}"
      items = name.chars

      if items.empty? || 63 < items.length
        if echo
          Color.echo("'#{name}' should be between 1 and 63 characters long", color)
        end

        return false
      end

      if [items.first, items.last].include?('-')
        if echo
          Color.echo("'#{name}' should not start or end with a hyphen(-)", color)
        end

        return false
      end

      test = name.match?(/^[a-zA-Z0-9-]+$/)

      if false == test && echo
        Color.echo("'#{name}' should be a-z or A-Z or 0-9 and hyphen (-)", color)
      end

      return test
    end

    # @param [String] swarm
    def self.write_swarm(swarm = '')
      return File.write(self.swarm_path, "#{swarm}\n")
    end

    def self.read_swarm
      file = self.swarm_path
      return File.exist?(file) ? File.read(file).strip : ''
    end

    def self.detect_services
      return { docker: Shell.is_active_unit('docker') }
    end

    def self.detect_orchestrator
      swarm = self.read_swarm.to_sym
      detect = self.detect_services

      if detect.has_key?(swarm) && detect[swarm]
        return "#{swarm}"
      end

      index = detect.key(true)
      return "#{index}"
    end

    def self.swarm_status
      color = Color::GREEN
      Color.echo("Swarm: #{self.detect_orchestrator}", color)
      Color.echo(self.detect_services.to_yaml, color)
    end

    # @param [String] orchestrator
    def self.update_swarm(orchestrator = '')
      if false == orchestrator.empty?
        self.write_swarm(orchestrator)
      end

      orchestrator = self.detect_orchestrator
      self.write_swarm(orchestrator)

      if orchestrator.empty?
        orchestrator = '<none>'
      end

      Color.echo("Swarm: #{orchestrator}", Color::GREEN)
    end

    # @param [String] base
    # @param [String] relative
    def self.pair_paths(base, relative = '')
      items = {}

      { inside: base, outside: Dir.pwd }.each do |key, value|
        items[key] = File.join(value, relative)
      end

      return items
    end

    # @param [Array<String>] arguments
    # @param [Array<String>] folders
    def self.prepare_folders(*services, folders: %w[stack volume])
      deploy = self.deploy_path
      folders.each do |folder|
        services.each do |service|
          FileUtils.mkdir_p(File.join(deploy, folder, service))
        end
      end
    end

    #noinspection RubyUnusedLocalVariable
    # @param [Array] arguments
    # @param [Hash] environment
    # @param [Hash] bind
    # @param [Boolean] throw
    # @param [Numeric] wait
    # @param [Boolean] strip
    # @param [Boolean] echo
    # @return [String]
    def self.capture_command(*arguments, environment: {}, bind: {}, throw: false, wait: 0, strip: true, echo: false)
      Error.no_method(__method__)
    end

    #noinspection RubyUnusedLocalVariable
    # @param [Array] arguments
    # @param [Hash] environment
    # @param [Hash] bind
    # @param [Boolean] throw
    # @param [Numeric] wait
    # @param [Boolean] echo
    # @return [Boolean]
    def self.run_command(*arguments, environment: {}, bind: {}, throw: true, wait: 0, echo: true)
      Error.no_method(__method__)
    end

    #noinspection RubyClassVariableUsageInspection
    def self.orchestrator
      @@orchestrator ||= self.detect_orchestrator
      return "#{@@orchestrator}"
    end

  end

  module Paser
    # @param [String] text
    def self.boolean(text)
      return %w[true yes on].include?("#{text}".strip.downcase)
    end

    # @param [String] text
    def self.kebab(text)
      return "#{text}".gsub(/_/, '-')
    end

    # @param [Hash] hash
    def self.options(hash)
      items = []

      if hash.is_a?(Hash)
        hash.each do |key, value|
          short = 1 == key.length
          prefix = short ? '-' : '--'
          separator = short ? ' ' : '='
          name = prefix + self.kebab(key)

          if true == value
            items << name
            next
          end

          [value].flatten.each do |item|
            item = "#{item}".gsub(/\\/, '\&\&').gsub('"', '\"')
            items << %(#{name}#{separator}"#{item}")
          end
          next
        end
      end

      return items
    end

    # @param [Array] items
    def self.arguments(*items)
      list = []

      items.each do |item|
        if item.is_a?(Array)
          list.concat(item.map { |value| "#{value}" })
          next
        end

        if item.is_a?(Hash)
          list.concat(self.options(item))
          next
        end

        list << "#{item}"
        next
      end

      return list
    end
  end

  module System
    # @param [Numeric] second
    def self.wait(second = 0)
      if second.is_a?(Numeric)
        if 0 > second
          return sleep
        end
        if 0 < second
          return sleep(second)
        end
      end
    end

    # @param [Array] arguments
    # @param [Boolean, String] sudo
    def self.command(*arguments, sudo: SUDO)
      if true == sudo
        arguments.unshift('sudo')
      end

      if sudo.is_a?(String) && false == "#{sudo}".empty?
        arguments.unshift('su-exec', sudo)
      end

      return Paser.arguments(*arguments).join(' ')
    end

    # @param [Array] arguments
    # @param [Boolean, String] sudo
    # @param [Boolean] throw
    # @param [Numeric] wait
    # @param [Boolean] strip
    # @param [Boolean] echo
    def self.capture(*arguments, sudo: SUDO, throw: false, wait: 0, strip: true, echo: false)
      begin
        command = self.command(*arguments, sudo: sudo)

        if echo
          Color.echo(": #{command}", Color::YELLOW)
        end

        data = `#{command}`
        result = strip ? "#{data}".strip : data
        self.wait(wait)
        return result
      rescue
        raise unless throw
        return ''
      end
    end

    # @param [Array] arguments
    # @param [Boolean, String] sudo
    # @param [Boolean] throw
    # @param [Numeric] wait
    # @param [Boolean] echo
    def self.run(*arguments, sudo: SUDO, throw: true, wait: 0, echo: true)
      command = self.command(*arguments, sudo: sudo)

      if echo
        Color.echo("+ #{command}", Color::GREEN)
      end

      result = system(command, exception: throw)
      self.wait(wait)
      return result
    end

    # @param [Array] arguments
    # @param [Boolean] background
    # @param [Boolean, String] sudo
    # @param [Boolean] echo
    def self.execute(*arguments, background: false, sudo: SUDO, echo: true)
      command = self.command(*arguments, sudo: sudo)

      if echo
        Color.echo("= #{command}", Color::CYAN)
      end

      if background
        return spawn(command)
      else
        return exec(command)
      end
    end

    # @param [String] title
    # @param [Method] delegate
    # @param [Array] arguments
    def self.invoke(title, delegate, *arguments)
      color = Color::YELLOW
      Color.echo("> #{title}", color)
      delegate.call(*arguments)
      Color.echo("< #{title}", color)
    end

  end

  module Shell
    def self.architecture
      hash = { x86: '386', x86_64: 'amd64', armhf: 'armv6', armv7l: 'armv7', aarch64: 'arm64' }
      machine = "#{Etc.uname[:machine]}"
      return hash.fetch(machine.to_sym, machine)
    end

    # @param [String] unit
    def self.is_active_unit(unit)
      return 'active' == System.capture("systemctl is-active #{unit}").downcase
    end

    # @param [Array<String>] arguments
    # @return [Array<String>]
    def self.find_paths(*arguments)
      return Dir.glob(arguments.flatten, File::FNM_DOTMATCH).map do |item|
        next File.path(item)
      end.filter do |item|
        next false == %w[. ..].include?(File.basename(item))
      end.uniq
    end

    # @param [Array<String>] arguments
    # @param [String] user
    # @param [String] group
    def self.change_owner(*arguments, user: USER, group: GROUP)
      self.find_paths(*arguments).each do |path|
        FileUtils.chown_R(user, group, path, force: true)
      end
    end

    # @param [Array<String>] arguments
    # @param [String, Integer] mode
    def self.change_mode(mode, *arguments)
      self.find_paths(*arguments).each do |path|
        FileUtils.chmod_R(mode, path, force: true)
      end
    end

    # @param [Array<String>] arguments
    # @param [Integer, nil] mode
    def self.make_folders(*arguments, mode: nil)
      stack = []
      paths = arguments.map do |path|
        next File.path(path)
      end.uniq

      paths.each do |path|
        until path == stack.last
          stack << path
          path = File.dirname(path)
        end
      end

      stack.uniq.each do |path|
        if File.exist?(path) && false == File.directory?(path)
          File.delete(path)
        end
      end

      FileUtils.mkdir_p(paths, mode: mode)
    end

    # @param [Array<String>] arguments
    def self.remove_paths(*arguments)
      self.find_paths(*arguments).each do |path|
        FileUtils.rm_rf(path)
      end
    end

    # @param [String, Array<String>] source
    # @param [String] target
    def self.move_paths(source, target)
      paths = [source].flatten
      FileUtils.mv(self.find_paths(*paths), target)
    end

    # @param [String] uri
    # @param [Integer] max_redirect
    def self.resolve_link(uri, max_redirect = 10)
      response = Net::HTTP.get_response(URI.parse(uri))

      if false == response.is_a?(Net::HTTPRedirection)
        return uri
      end

      max_redirect = max_redirect - 1

      if 0 == max_redirect
        Error.maximum_redirect
      end

      return self.resolve_link(response['location'], max_redirect)
    end

    # @param [String] uri
    # @param [String] file
    def self.download_file(uri, file)
      uri = self.resolve_link(uri)
      content = Net::HTTP.get(URI(uri))
      return File.binwrite(file, content)
    end

    # @param [Object] io
    # @param [String] path
    # @param [Boolean] echo
    def self.unpack_archive(io, path, echo: false)
      Gem::Package::TarReader.new(io) do

        # @type [Gem::Package::TarReader] reader
      |reader|
        reader.rewind
        reader.each do |entry|
          # @type [Gem::Package::TarHeader] header
          header = entry.header
          mode = header.mode
          file = File.join(path, entry.full_name)

          if echo
            Color.echo(": #{file}", Color::GREEN)
          end

          if entry.directory?
            self.make_folders(file, mode: mode)
            next
          end

          if entry.file?
            self.make_folders(File.dirname(file))
            File.binwrite(file, entry.read)
            self.change_mode(mode, file)
            next
          end
        end
      end
    end

    # @param [String] file
    # @param [String] path
    # @param [Boolean] echo
    def self.inflate_file(file, path, echo: false)
      Zlib::GzipReader.open(file) do

        # @type [Zlib::GzipReader] reader
      |reader|
        self.unpack_archive(reader, path, echo: echo)
      end
    end

    # @param [String] path
    # @param [Boolean] echo
    def self.tape_archive(path, echo: false)
      io = StringIO.new('')
      offset = path.size + 1

      Gem::Package::TarWriter.new(io) do

        # @type [Gem::Package::TarWriter] writer
      |writer|
        self.find_paths(File.join(path, '**/*')).each do |name|
          mode = File.stat(name).mode
          file = name.slice(offset ..)

          if echo
            Color.echo(": #{file}", Color::GREEN)
          end

          if File.directory?(name)
            writer.mkdir(file, mode)
            next
          end

          if File.file?(name)
            writer.add_file(file, mode) do |stream|
              stream.write(File.binread(name))
            end
            next
          end
        end
      end

      io.rewind
      return io.string
    end

    # @param [String] file
    # @param [String] path
    # @param [Boolean] echo
    def self.deflate_file(file, path, echo: false)
      Zlib::GzipWriter.open(file, Zlib::BEST_COMPRESSION) do

        # @type [Zlib::GzipWriter] writer
      |writer|
        writer.write(self.tape_archive(path, echo: echo))
      end
    end

    # @param [Integer] uid
    # @param [Integer] gid
    def self.update_user(uid: 500, gid: 500)
      uid = ENV.fetch('DOCKER_CORE_UID', uid)
      gid = ENV.fetch('DOCKER_CORE_GID', gid)

      System.run('deluser', USER, throw: false)
      System.run('delgroup', GROUP, throw: false)
      System.run('addgroup', { system: true, gid: gid }, GROUP)
      System.run('adduser', { system: true, 'disabled-password': true, 'no-create-home': true, ingroup: GROUP, gecos: USER, shell: '/bin/bash', uid: uid }, USER)
    end

    # @param [String] stdout
    # @param [String] stderr
    def self.link_logs(stdout: '', stderr: '')
      if '' != stdout
        File.symlink('/dev/stdout', File.path(stdout))
      end

      if '' != stderr
        File.symlink('/dev/stderr', File.path(stderr))
      end

      return
    end

    # @param [Array<String>] arguments
    # @param [Integer ,nil] mode
    def self.clear_folders(*arguments, mode: nil)
      self.remove_paths(*arguments)
      self.make_folders(*arguments, mode: mode)
    end

    # @param [String] repository
    def self.github_latest_version(repository)
      uri = URI("https://api.github.com/repos/#{repository}/releases/latest")
      data = JSON.parse(Net::HTTP.get(uri))['tag_name']
      return "#{data}"

    end
  end

end
